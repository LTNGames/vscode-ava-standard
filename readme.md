# AVA Standard Style Snippets
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

> Standard snippets for [AVA](https://ava.li/)
[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

Forked from [vscode-ava](https://github.com/SamVerschueren/vscode-ava)

## Install

```
$ ext install ava-standard
```

Or, `F1` → `Install Extension` → Search for `ava-standard`


## Snippets

Included are some [test](snippets/ava.json) and [assertion](snippets/assertions.json) snippets useful for writing AVA tests.

Start writing a snippet's `prefix` and then press <kbd>Tab ↹</kbd> to expand the snippet.


## Related

- [Sublime plugin](https://github.com/sindresorhus/sublime-ava)
- [Atom plugin](https://github.com/sindresorhus/atom-ava)


## License

MIT © [Sam Verschueren](https://github.com/SamVerschueren)
